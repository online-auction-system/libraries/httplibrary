﻿using Newtonsoft.Json.Serialization;

namespace OnlineAuctionSystem.HttpLibrary.ContractResolvers
{
    public class SnakeCaseContractResolver : DefaultContractResolver
    {
        public SnakeCaseContractResolver()
        {
            NamingStrategy = new SnakeCaseNamingStrategy
            {
                ProcessDictionaryKeys = true,
                OverrideSpecifiedNames = true
            };
        }
    }
}