﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OnlineAuctionSystem.HttpLibrary
{
    public static class HttpContentExtensions
    {
        private static readonly ConcurrentDictionary<Type, JsonSerializer> JsonSerializers =
            new ConcurrentDictionary<Type, JsonSerializer>();

        public static async Task<T> ReadAsAsync<T, TContractResolver>(this HttpContent content)
            where TContractResolver : IContractResolver, new()
        {
            if (typeof(T) == typeof(string))
            {
                var str = await content.ReadAsStringAsync().ConfigureAwait(false);
                return (T) (object) str;
            }

            await using var stream = await content.ReadAsStreamAsync().ConfigureAwait(false);
            var serializer = GetSerializer<TContractResolver>();
            using var streamReader = new StreamReader(stream);
            using JsonReader jsonReader = new JsonTextReader(streamReader);
            return serializer.Deserialize<T>(jsonReader);
        }

        private static JsonSerializer GetSerializer<TContractResolver>()
            where TContractResolver : IContractResolver, new()
        {
            return JsonSerializers.GetOrAdd(typeof(TContractResolver),
                t => new JsonSerializer {ContractResolver = new TContractResolver()});
        }
    }
}