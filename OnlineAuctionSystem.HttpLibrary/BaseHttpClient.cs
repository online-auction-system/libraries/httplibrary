﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OnlineAuctionSystem.Entities;
using OnlineAuctionSystem.Entities.Exceptions;

namespace OnlineAuctionSystem.HttpLibrary
{
    public abstract class BaseHttpClient
    {
        private HttpClient _client;
        protected abstract string ServiceName { get; }
        protected abstract Services ServiceId { get; }
        protected abstract string BaseUrl { get; }

        protected readonly ILogger Logger;


        protected BaseHttpClient(
            ILogger logger)
        {
            Logger = logger;
        }

        protected void CreateHttpClient()
        {
            var client = new HttpClient
            {
                Timeout = Timeout.InfiniteTimeSpan
            };
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            _client = client;
        }

        protected void RemoveHeader(string name)
        {
            _client.DefaultRequestHeaders.Remove(name);
        }

        protected void AddHeader(string name, string value)
        {
            _client.DefaultRequestHeaders.Add(name, value);
        }

        protected Task SendAsync(
            HttpRequestMessage msg,
            TimeSpan timeout,
            List<int> allowedStatusCodes = null)
        {
            return SendAsync<bool, DefaultContractResolver>(msg, timeout, allowedStatusCodes, false);
        }

        protected async Task<T> SendAsync<T, TContractResolver>(
            HttpRequestMessage msg,
            TimeSpan timeout,
            List<int> allowedStatusCodes = null,
            bool getAnswer = true) where TContractResolver : IContractResolver, new()
        {
            using var respMsg =
                await OasSendAsync(msg, timeout, allowedStatusCodes).ConfigureAwait(false);
            if (!getAnswer)
            {
                return default;
            }

            try
            {
                var responseObject =
                    await respMsg.Content.ReadAsAsync<T, TContractResolver>().ConfigureAwait(false);
                return responseObject;
            }
            catch (JsonSerializationException ex)
            {
                var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                Logger.LogError(0, ex,
                    $"Не удалось десериализовать данные от сервиса '{ServiceName}' при обращении к урлу '{msg.RequestUri}', ошибка: '{ex.Message}'{requestBodyLog}");
            }
            catch (Exception ex)
            {
                var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                Logger.LogError(0, ex,
                    $"Необработанная ошибка при обработке ответа от сервиса '{ServiceName}' по урлу '{msg.RequestUri}'{requestBodyLog}");
            }

            throw new ServiceUnavaliableException(ServiceId);
        }

        protected async Task<HttpResponseMessage> OasSendAsync(
            HttpRequestMessage msg,
            TimeSpan timeout,
            List<int> allowedStatusCodes = null,
            bool suppressErrors = false)
        {
            var relativePath = msg.RequestUri;

            if (relativePath.IsAbsoluteUri)
            {
                throw new ArgumentException($"В сообщении указан абсолютный путь {msg.RequestUri}");
            }

            try
            {
                var baseUri = GetBaseUriAsync();
                msg.RequestUri = new Uri(baseUri, relativePath);

                HttpResponseMessage respMsg;
                using (var cts = new CancellationTokenSource(timeout))
                {
                    respMsg = await _client.SendAsync(msg, HttpCompletionOption.ResponseHeadersRead, cts.Token)
                        .ConfigureAwait(false);
                }

                if (respMsg == null)
                {
                    Logger.LogError(
                        $"Не удалось отправить запрос к сервису '{ServiceName}'.");
                }
                else if (respMsg.IsSuccessStatusCode ||
                         allowedStatusCodes != null && allowedStatusCodes.Contains((int) respMsg.StatusCode))
                {
                    return respMsg;
                }
                else
                {
                    var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                    Logger.LogError(
                        $"При запросе к сервису '{ServiceName}' получили плохой код ответа '{(int) respMsg.StatusCode}' при обращении к урлу {msg.RequestUri}{requestBodyLog}");
                }
            }
            catch (TaskCanceledException)
            {
                var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                Logger.LogError(
                    $"Запрос к сервису '{ServiceName}' отпал по таймауту при обращении к урлу {msg.RequestUri}{requestBodyLog}");
            }
            catch (HttpRequestException ex)
            {
                var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                Logger.LogError(0, ex,
                    $"Не удалось получить ответ от сервиса '{ServiceName}' - '{ex.Message}'{requestBodyLog}");
            }
            catch (Exception ex)
            {
                var requestBodyLog = await GetLogAppendixWithRequestBodyAsync(msg).ConfigureAwait(false);
                Logger.LogError(0, ex,
                    $"Необработанная ошибка при обращении к сервису '{ServiceName}' по урлу '{msg.RequestUri}'{requestBodyLog}");
            }
            finally
            {
                // КОСТЫЛЬ!!!
                // После выполнения присваиваем относительный путь. Надо для повторной отправки запроса,
                // чтобы не падало с throw new ArgumentException($"В сообщении указан абсолютный путь {msg.RequestUri}");
                msg.RequestUri = relativePath;
            }


            if (suppressErrors)
            {
                return null;
            }

            // Мы залогировали причину, далее нам уже не важно, почему произошел сбой, важен сам факт
            throw new ServiceUnavaliableException(ServiceId);
        }

        private Uri GetBaseUriAsync()
        {
            if (!string.IsNullOrEmpty(BaseUrl))
            {
                return new Uri(BaseUrl);
            }

            throw new FinalException(500, "Empty service baseUrl");
        }

        private static async Task<string> GetLogAppendixWithRequestBodyAsync(HttpRequestMessage msg)
        {
            if (msg == null || msg.Method == HttpMethod.Get || msg.Content == null)
            {
                return null;
            }

            var requestBody = await msg.Content.ReadAsStringAsync().ConfigureAwait(false);
            return !string.IsNullOrEmpty(requestBody) ? $" Тело запроса:{requestBody}" : null;
        }

        protected static HttpContent CreateContent<TInput>(TInput input, JsonSerializer resolver)
        {
            if (input == null)
            {
                return null;
            }

            byte[] body;
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var jsonTextWriter = new JsonTextWriter(streamWriter))
                    {
                        resolver.Serialize(jsonTextWriter, input);
                        streamWriter.Flush();
                        body = memoryStream.ToArray();
                    }
                }
            }
            var result = new ByteArrayContent(body);
            var headerValue = new MediaTypeHeaderValue("application/json") {CharSet = Encoding.UTF8.WebName};
            result.Headers.ContentType = headerValue;

            return result;
        }
    }
}